class AddShipDataToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :ship_data, :datetime, default: Time.now.strftime('%y/%m/%d %H:%M:%S')
  end
end
