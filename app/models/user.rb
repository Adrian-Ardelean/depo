class User < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  after_destroy :ensure_an_admin_remains
  has_secure_password

  private

  def ensure_an_admin_remains
    raise Error.new "Can't delete the last user" if User.count.zero?
  end
end

class Error < StandardError
end
