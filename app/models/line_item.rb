class LineItem < ApplicationRecord
  belongs_to :order, optional: true
  belongs_to :product, optional: true
  belongs_to :card

  def total_price
    product.price * quantity
  end
end
