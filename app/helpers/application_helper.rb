module ApplicationHelper

  def show_time
    Time.now.strftime('%m/%d/%Y %H:%M:%S')
  end

  def render_if(condition, record)
    if condition
      render record
    end
  end

end
