module AdminHelper

  def default_user
    user = User.find_by(id: session[:user_id])
    'admin/default_user' if user.name.eql? 'Admin'
  end
end
