class StoreController < ApplicationController
  skip_before_action :authorize

  include SessionMethod
  include CurrentCard
  before_action :set_card
  def index
    @products = Product.order(:title)
    @counter = count_index
  end
end
