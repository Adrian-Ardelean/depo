module SessionMethod
  def count_index
    if session[:counter].nil?
      session[:counter] = 1
    else
      session[:counter] += 1
    end
  end

  def reset_count
    session[:counter] = 0
  end

end