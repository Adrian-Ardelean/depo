require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  fixtures :products
  test 'Product attributes must not be null' do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:image_url].any?
    assert product.errors[:price].any?
  end

  test 'Product price must be positive' do
    product = Product.new(
                         title: 'Cana',
                         description: 'Cana frumoasa',
                         image_url: 'cana.jpg'
    )
    product.price = -1
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"],
                 product.errors[:price]

    product.price = 0
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"],
                 product.errors[:price]

    product.price = 1
    assert product.invalid?
  end

  def new_product image_url
    Product.new(
               title: 'Cana',
               description: 'Cana frumoasa',
               price: 1,
               image_url: image_url
    )
  end

  test 'Image_url' do
    ok = %w{ fred.gif fred.jpg fred.png FRED.JPG FRED.Jpg http://a.b.c/x/y/z/fred.gif }
    bad = %w{ fred.doc fred.gif/more fred.gif.more }
    ok.each do |image_url|
      assert new_product(image_url).invalid?,
             "#{image_url} shouldn't be invalid"
    end
    bad.each do |image_url|
      assert new_product(image_url).invalid?,
             "#{image_url} shouldn't be valid"
    end
  end

  test 'Product title must be unique' do
    product = Product.new(
                         title: products(:ruby).title,
                         description: 'ruby',
                         image_url: 'fred.jpg',
                         price: 1
    )
    assert product.invalid?
    assert_equal ['has already been taken'], product.errors[:title]
  end
end
