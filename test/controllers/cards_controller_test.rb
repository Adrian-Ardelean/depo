require 'test_helper'

class CardsControllerTest < ActionDispatch::IntegrationTest
  fixtures :products
  setup do
    @card = cards(:one)
  end

  test "should get index" do
    get cards_url
    assert_response :success
  end

  test "should get new" do
    get new_card_url
    assert_response :success
  end

  test "should create card" do
    assert_difference('Card.count') do
      post cards_url, params: { card: {  } }
    end

    assert_redirected_to card_url(Card.last)
  end

  test "should show card" do
    get card_url(@card)
    assert_response :success
  end

  test "should get edit" do
    get edit_card_url(@card)
    assert_response :success
  end

  test "should update card" do
    patch card_url(@card), params: { card: {  } }
    assert_redirected_to card_url(@card)
  end

  test "should destroy card" do
    post line_items_url, params: { product_id: products(:ruby).id }
    @card = Card.find(session[:card_id])
    assert_difference('Card.count', -1) do
      delete card_url(@card)
    end

    assert_redirected_to store_index_url
  end
end
